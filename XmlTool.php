<?php

use SimpleXMLElement;

/**
 * XML工具
 * Class XmlTool by meco.lee
 * email: liwenhao12888@163.com
 */
class XmlTool
{
    /**
     * 数组转XML
     * 使用说明: echo XmlTool::array2xml($arr);
     * @param array $arr
     * @param string $root_element_name
     * @param bool $noDecl 是否包含声明 <?xml version="1.0" encoding="utf-8"?>
     * @return mixed
     */
    public static function array2xml(array $arr,$root_element_name='Request',$noDecl=true)
    {
        $xmlElement = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"utf-8\"?><{$root_element_name}></{$root_element_name}>");
        $f=function ($f,$xml,$array){
            if ( is_array( $array ) ) {
                foreach( $array as $key => $value ) {
                    if ( is_int( $key ) ) {
                        if ( $key == 0 ) {
                            $node = $xml;
                        } else {
                            $parent = $xml->xpath( ".." )[0];
                            $node = $parent->addChild( $xml->getName() );
                        }
                    } else {
                        $node = $xml->addChild( $key );
                    }
                    $f($f, $node, $value );
                }
            } else {
                $xml[0] = $array;
            }
        };
        if ($noDecl){
            $f($f,$xmlElement,[$root_element_name=>$arr]);
            $aa=$xmlElement->xpath($root_element_name);
            $str=$aa[0]->saveXML();
        }else{
            $f($f,$xmlElement,$arr);
            $str=$xmlElement->saveXML();
        }
        return $str;
    }

    /**
     * XML转数组
     * 示例数据: <Request><code>1</code><msg>22</msg><mydata><list><item><order>1</order><name>姓名</name></item></list></mydata><data><list><item><order>2</order><name>2</name></item><item><order>3</order><name>3</name></item></list></data></Request>
     * 使用说明: $arr = XmlTool::xml2array($xml,["/mydata/list/item"]);
     * @param $xml
     * @param array $listPaths 把节点当作列表,转换成0下标数组 $a=['a'=>1,'b'=>2];  $a[0]=['a'=>1,'b'=>2];
     * @return array|string
     */
    public static function xml2array($xml,$listPaths=[])
    {
        if (empty($xml)) {
            return [];
        }
        $array = (simplexml_load_string($xml,'SimpleXMLElement',LIBXML_NOCDATA ));
        $f = function ($f,$item,$listPaths=[],$path=''){
            if (! is_string ( $item )) {
                $item = ( array ) $item;
                foreach ( $item as $key => $val ) {
                    if ($listPaths){
                        if(!is_int($key)&&! is_string ( $val )){
                            $pathTmp ="$path/$key";
                        }else{
                            $pathTmp =$path;
                        }
                        if(in_array($pathTmp,$listPaths)){
                            if(!is_int($key)&&is_object($val)){
                                $valTemp[0]=$val;
                            }else{
                                $valTemp=$val;
                            }
                        }else{
                            $valTemp=$val;
                        }
                    }else{
                        $pathTmp =$path;
                        $valTemp=$val;
                    }
                    $item [$key] = $f($f, $valTemp ,$listPaths,$pathTmp);
                }
            }
            return $item;
        };
        $array=$f($f,$array,$listPaths);
        return $array;
    }
}
