# xmltool

#### 介绍
PHP 解析xml工具 array2xml xml2array 

#### 使用说明
```php
$xml="<rsp>
	<code>1</code>
	<msg>22</msg>
	<mydata>
		<list>
			<item>
				<order>1</order>
				<name>姓名</name>
			</item>
		</list>
	</mydata>
	<data>
		<list>
			<item>
				<order>2</order>
				<name>2</name>
			</item>
			<item>
				<order>3</order>
				<name>3</name>
			</item>
		</list>
	</data>
</rsp>";
        $arr = XmlTool::xml2array($xml,["/mydata/list/item"]);
        var_dump($arr);
        echo XmlTool::array2xml($arr);

```
